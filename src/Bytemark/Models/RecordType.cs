﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;

namespace AcmeDns01.Bytemark.Models
 {
    [JsonConverter(typeof(StringEnumConverter))]
    public enum RecordType
    {
        A, AAAA, CNAME, MX, NS, SOA, SRV, TXT
    }
}
