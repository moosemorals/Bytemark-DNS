﻿using Newtonsoft.Json;

namespace AcmeDns01.Bytemark.Models
 {
    internal class ErrorResponse
    {
        public ErrorResponse(string error)
        {
            Error = error;
        }

        [JsonProperty("error")]
        public string Error { get; set; }
    }
}
