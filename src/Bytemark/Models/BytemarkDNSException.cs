﻿using System;

namespace AcmeDns01.Bytemark.Models
 {
    internal class BytemarkDNSException : Exception
    {
        public BytemarkDNSException(string message) : base(message)
        {
        }

        public BytemarkDNSException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
