﻿using System.Threading.Tasks;
using AcmeDns01.Bytemark;
using Microsoft.Extensions.Configuration;

namespace AcmeDns01
 {
    public interface ICommand
    {
        public const int Fail = 1;
        public const int Success = 0;
        public string Noun { get; }

        public string Verb { get; }

        public string Usage { get; }

        public Task<int> Execute(
            BytemarkDNSClient client,
            IConfiguration config
            );

    }
}
